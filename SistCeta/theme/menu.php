	<!--*********** cambio de hojas de estilo ***************-->
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <!-- ************** Menu ********************************-->
    <link rel="stylesheet" type="text/css" href="css/superfish.css" media="screen">
	<!-- Select's -->
    <script type="text/javascript" src="theme/js/jQuery.js"></script>
	<!--   Slide   -->
	<script type="text/javascript" src="theme/slide/slide.js"></script>
	<script type="text/javascript" src="theme/js/funciones.js"></script>
    <!-- ************** Menu ********************************-->
    <script type="text/javascript" src="theme/js/hoverIntent.js"></script>
	<script type="text/javascript" src="theme/js/superfish.js"></script>

<table align="center" border="0">
<tr>
<td height="70">
<img src=imgs/header1.png alt="" width="1235" height="90" />
</td>
</tr>
<tr>
<td>

<!-- Menu -->
<div align="center">
<div id="navigator">

<ul class="sf-menu">
			
 <!-- <li class="current"> <a href="">Inicio</a> 
      <ul>
        <li> <a href="header_inicio.php">Principal</a> </li>
        <li> <a href="login.php">Salir</a> </li>
      </ul>
    </li>-->
			
			
    <li> <a href="">Administrativo</a> 
      <ul>
        <li> <a href="">Inscripciones</a> 
          <ul>
            <li><a href="Administrativo/Inscripciones/reg_inscripcion_estnuevos.php">Inscripciones de estudiantes nuevos</a></li>
            <li><a href="Administrativo/Inscripciones/reg_inscripcion_est_antiguos.php">Inscripciones de estudiantes antiguos</a></li>
          </ul>
        </li>
        <li> <a href="">Modificar inscripción</a> 
          <ul>
            <li><a href="Administrativo\Modificar_inscripcion\modif_inf_estudiantes.php">Modificar información del estudiante</a></li>
            <li><a href="Administrativo/Modificar_inscripcion/modif_inscripcion_est.php">Modificar inscripción del estudiante</a></li>
            <li><a href="Administrativo/Modificar_inscripcion/modif_estado_est.php">Modificar estado del estudiante</a></li>
            <li><a href="Administrativo/Modificar_inscripcion/modif_estado_est_xgrupo.php">Modificar estado del estudiante por grupo</a></li>
            
          </ul>
        </li>
        <li> <a href="">Cobros/Ventas</a> 
          <ul>
            <li><a href="Administrativo/Cobros_ventas/cobro_matricula.php">Cobro de matrícula</a></li>
            <li><a href="Administrativo/Cobros_ventas/cobro_mensualidad.php">Cobro de mensualidad</a></li>
            <li><a href="Administrativo/Cobros_ventas/venta_seg_inst.php">Venta de 2da instancia </a></li>
             <li><a href="Administrativo/Cobros_ventas/venta_seg_inst.php">Venta de boleta rezagados </a></li>
             <li><a href="Administrativo/Cobros_ventas/venta_material_academico.php">Venta de material académico </a></li>
             <li><a href="Administrativo/Cobros_ventas/registro_ingresos(ventas).php">Registro de otros ingresos(ventas) </a></li>
             <li><a href="Administrativo/Cobros_ventas/registro_egresos(compras).php">Registro de otros egresos(compras) </a></li>
             <li><a href="Administrativo/Cobros_ventas/registro_facturas(extras).php">Registro de facturas extras </a></li>
          </ul>
        <li> <a href="">Modificar Cobros/Ventas</a> 
        <ul>
            <li><a href="Administrativo/Modificar_cobros_ventas/modificar_cobro_matricula.php">Modificar cobro de matrícula</a></li>
            <li><a href="Administrativo/Modificar_cobros_ventas/modificar_cobro_mensualidad.php">Modificar cobro de mensualidad</a></li>
            <li><a href="Administrativo/Modificar_cobros_ventas/modificar_venta_2dainst.php">Modificar venta de 2da instancia </a></li>
             <li><a href="Administrativo/Modificar_cobros_ventas/modificar_ingresos(ventas).php">Modificar otros ingresos(ventas) </a></li>
             <li><a href="Administrativo/Modificar_cobros_ventas/modificar_egresos(compras).php">Modificar otros egresos(compras) </a></li>
             <li><a href="Administrativo/Modificar_cobros_ventas/modificar_facturas_extras.php">Modificar facturas extras</a></li>
          </ul>
       
        </li>
      </ul>
                
    <li> <a href="">Académico</a> 
      <ul>
        <li> <a href="Academico/asignar_beca_convenio al estudiante.php">Asignar becas</a> </li>
        <li> <a href="Academico/cambiar_calificaciones_estudiantes.php">Cambiar calificaciones</a> </li>
         <li> <a href="Academico/registrar_convalidaciones.php">Convalidaciones</a> </li>
         <li> <a href="Academico/observacion_orden_examen.php">Observaciones/ orden de exámen</a> </li>
          <li> <a href="Academico/autorizaciones.php">Autorizaciones</a> </li>
        <li> <a href="Academico/modificar_ grupo_ estudiante.php">Modificar grupo del estudiante</a> </li>
        <li> <a href="Academico/registrar_criterio_evaluacion.php">Registrar criterio</a> </li>
        <li> <a href="Academico/modificar_criterio_evaluacion.php">Modificar criterio</a> </li>
        <li> <a href="Academico/eliminar_criterio_evaluacion.php">Eliminar criterio</a> </li>
        <li> <a href="Academico/registrar_evaluacion_materia.php">Registrar evaluación/materia</a> </li>
        <li> <a href="Academico/modificar_evaluacion_materia.php">Modificar evaluación/materia</a> </li>
        <li> <a href="Academico/eliminar_evaluacion_materia.php">Eliminar evaluación/materia</a> </li>
      </ul>
    </li>
             
    <li> <a href="">Económico</a> 
      <ul>
        <li> <a href="Economico/registrar_razon_social.php">Definir razón social</a> </li>
        <li> <a href="Economico/modificar_razon_social.php">Modificar razón social</a> </li>
        <li> <a href="Economico/eliminar_razon_social.php">Eliminar razón social</a> </li>
        <li> <a href="Economico/registro_dosificacion_de_facturas.php">Definir dosificación</a> </li>
        <li> <a href="Economico\modificar_dosificacion_de_facturas.php">Modificar dosificación</a> </li>
        <li> <a href="Economico/eliminar_dosificacion_de_facturas.php">Eliminar dosificación</a> </li>
        <li> <a href="Economico/registrar_caja_usuario.php">Definir caja/usuario</a> </li>
        <li> <a href="Economico/modificar_caja_usuario.php">Modificar caja/usuario</a> </li>
        <li> <a href="Economico/eliminar_caja_usuario.php">Eliminar caja/usuario</a> </li>
              
              
      </ul>
    </li>
    
     <li> <a href="">Aplicaciones avanzadas</a> 
      <ul>
        <li> <a href="Aplicaciones_avanzadas/registrar_gestion.php">Gestión</a> </li>
        <li> <a href="Aplicaciones_avanzadas/registrar_costos_académicos.php">Costos</a> </li>
        <li> <a href="Aplicaciones_avanzadas/registrar_grupos.php">Grupos</a> </li>
        <li> <a href="Aplicaciones_avanzadas/registrar_usuario.php">Registrar usuarios</a> </li>
        <li> <a href="Aplicaciones_avanzadas/modificar_usuario.php">Modificar usuarios</a> </li>
        <li> <a href="Aplicaciones_avanzadas/eliminar_usuario.php">Eliminar usuarios</a> </li>
        <li> <a href="Aplicaciones_avanzadas/eliminar_datos_estudiante.php">Eliminar estudiantes</a> </li>
        <li> <a href="Aplicaciones_avanzadas/inscripcion_casos_especiales.php">Inscripción casos especiales</a> </li>
        <li> <a href="Aplicaciones_avanzadas/depuracion_deudores_mensuales.php">Depuración deudores de mensualidad</a> </li>
        <li> <a href="Aplicaciones_avanzadas/actualizar_matriculas.php">Actualizar matricula</a> </li>
                     
       </ul>
    </li>
    
    <li> <a href="">Centralizadores</a> 
      <ul>
        <li> <a href="Centralizadores/generador_centralizadores_internos.php">Centralizadores internos</a> </li>
        <li> <a href="Centralizadores/generador_centralizadores_internos -2daInst.php">Centralizadores internos 2da Instancia</a> </li>
        <li> <a href="Centralizadores/generador_centralizadores_DDE.php">Centralizadores internos de DDE</a> </li>
        <li> <a href="Centralizadores/generador_centralizadores_internos -2daInst.php">Centralizadores internos de DDE 2da Instancia</a> </li>
                            
      </ul>
    </li>
    <li> <a href="">Reimpresiones</a> 
      <ul>
        <li> <a href="Reimpresiones/reimpresion_inscripcion.php">Reimpresión inscripción</a> </li>
        <li> <a href="Reimpresiones/reimpresion_boletas.php">Reimpresión boletas</a> </li>
        
                            
      </ul>
    </li>
         <li> <a href="">Planillas</a> 
      <ul>
        <li> <a href="Planillas/generar_planillas_calificaciones.php">Generar planilla calificaciones</a> </li>
        <li> <a href="Planillas/generar_2dainst_xgrupo.php">Generar 2da instancias por grupo</a> </li>
        <li> <a href="Planillas/generar_estudiantes_sin_calificaciones.php">Generar estudiantes sin calificaciones</a> </li>
        <li> <a href="Planillas/generar_asignacion_docentes.php">Generar asignación docentes</a> </li>
        <li> <a href="Planillas/generar_control_cuotas.php">Generar control cuotas</a> </li>
        <li> <a href="Planillas/generar_compras_ventas.php">Generar compras ventas</a> </li>
        <li> <a href="Planillas/carga_planilla_calificaciones.php">Carga de planilla de calificaciones</a> </li>
        <li> <a href="Planillas/carga_estudantes_sin_calificaciones.php">Carga estudiantes sin calificaciones</a> </li>
        <li> <a href="Planillas/carga_cronograma_evaluaciones.php">Carga cronograma de evaluación</a> </li>
        <li> <a href="Planillas/carga_asignacion_docentes.php">Carga asignación docente</a> </li>
                            
        </ul>
       </li>
       
       <li> <a href="">Reportes</a> 
      <ul>
        <li> <a href="">Calificaciones</a> 
          <ul>
            <li><a href="Reportes/Calificaciones/informe_calificaciones_estudiente.php">Informe de calificaciones/estudiante</a></li>
            <li><a href="Reportes/Calificaciones/informe_calificaciones_por_grupo.php">Informe de calificaciones/grupo</a></li>
          </ul>
        </li>
        <li> <a href="">Académico</a> 
          <ul>
            <li><a href="Reportes/Academico/plan_estudios.php">Plan de estudios</a></li>
            <li><a href="Reportes/Academico/plan_estudios.php">Docentes</a></li>
            <li><a href="Reportes/Academico/cantidad_estudiantes_inscritos.php">Cantidad de inscritos</a></li>
            <li><a href="Reportes/Academico/correccion_calificaciones.php">Corrección calificaciones</a></li>
            
          </ul>
        </li>
        <li> <a href="">Económico</a> 
          <ul>
            <li><a href="Reportes/Economico/Resumen_compras_ventas/resumen_compras_ventas.php">Resumen de Compras/Ventas</a></li>
            <li><a href="Reportes/Economico/Libro_diario_xusuario/reporte_libro_diario_xusuario.php">Libro diario por usuario</a></li>
            <li><a href="Reportes/Economico/Libro_diario/reporte_libro_diario.php">Libro diario </a></li>
             <li><a href="Reportes/Economico/Deudores_matricula/deudores_matricula.php">Deudores de matrícula</a></li>
             <li><a href="Reportes/Economico/deudores_mensualidad.php">Deudores de mensualidad </a></li>
             <li><a href="Reportes/Economico/costos_académicos.php">Costos académicos</a></li>
             <li><a href="Reportes/Economico/costos_académicos.php">Libros textos a la venta </a></li>
             <li><a href="Reportes/Economico/resportes_detallados.php">Reportes detallados </a></li>
          </ul>
        <li> <a href="">Listas de estudiantes</a> 
        <ul>
            <li><a href="Reportes/Listas estudiantes/control_asistencia_xgrupo.php">Control de asistecia</a></li>
            <li><a href="Reportes/Listas estudiantes/estudiantes_inscritos_xgrupo.php">Estudiantes inscritos por grupo</a></li>
            <li><a href="Reportes/Listas estudiantes/Estudiantes_con_arrastres/estudiantes_con_arrastres.php">Estudiantes con arrastre </a></li>
             <li><a href="Reportes/Listas estudiantes/Estudiante_abandono/estudiantes_en_abandono.php">Estudiates en estado de abandono </a></li>
             <li><a href="Reportes/Listas estudiantes/Estudiantes_rezagados/estudiantes_rezagados.php">Estudiantes rezagados </a></li>
             <li><a href="Reportes/Listas estudiantes/estudiantes_con_segundas_instancias.php">Estudiantes con segundas instancias </a></li>
             <li><a href="Reportes/Listas estudiantes/actualizar_biografia.php">Actualización con información biográfica </a></li>
          </ul>
       
        </li>
      </ul>
      <li> <a href="">Herramientas</a> 
      <ul>
        <li> <a href="Herramientas/buscar_facturas_recibos.php">Busqueda de facturas</a> </li>
        <li> <a href="Herramientas/tomar_fotografia.php">Tomar fotografías</a> </li>
        <li> <a href="../mod_creditos/creditos.php">Cámara de vigilencia</a> </li>
        <li> <a href="../mod_creditos/creditos.php">www.ceta.edu.bo</a> </li>
        <li> <a href="../mod_creditos/creditos.php">Acerca de.....</a> </li>
                                           
        </ul>
    </li>
       
        <li> <a href="">Recursos Humanos</a> 
        <ul>
        <li> <a href="Recursos_humanos/registro_personal.php">Registro de personal</a> </li>
        <li> <a href="Recursos_humanos/actualizar_registro_personal.php">Actualizar registro de personal</a> </li>
        <li> <a href="Recursos_humanos/eliminar_registro_personal.php">Eliminar registro de personal</a> </li>
        <li> <a href="Recursos_humanos/asignar_cargo.php">Asignar cargo</a> </li>
                                                
        </ul>
        </li>
        
        <li> <a href="">DDE</a> 
        <ul>
        <li> <a href="DDE/libro_inscripciones.php">Libro de Inscripciones</a> </li>
                                                    
        </ul>
        <li> <a href="">Capacitación</a> 
        <ul>
        <li> <a href="Capacitacion/cronograma_capacitacion.php">Cronograma de capacitación</a> </li>
        
        <li> <a href="">Formularios de interes</a> 
          <ul>
            <li><a href="Capacitacion/formulario_electronica.php">Formulario de Electrónica</a></li>
            <li><a href="Capacitacion/formulario_mecanica.php">Formulario de Mecánica</a></li>
          </ul>
        </li>
                      
        <li> <a href="Administrativo/Inscripciones/reg_inscripcion_estnuevos.php">Inscribirse a curso(estudiantes)  </a> </li> <li> <a href="Capacitacion/reg_inscripcion_externos.php">Inscribirse a curso(externos)</a> </li>                                  
        </ul>
    </li>
</ul>
</div>
</div>

</td>
</tr>
</table>
