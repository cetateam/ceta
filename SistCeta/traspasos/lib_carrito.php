<? //session_start();
	
class carrito {
	//atributos de la clase
   	var $num_productos;
   	var $array_id_prod;
	var $array_llave;
	var $array_nombre_prod;
	//var $array_tamano;
	var $array_color;
	var $array_cant_prod;
   	var $array_precio_prod;
 	var $array_cod_venta;
	//constructor. Realiza las tareas de inicializar los objetos cuando se instancian
	//inicializa el numero de productos a 0
	function carrito () {
   		$this->num_productos=0;
	}
	
	//Introduce un producto en el carrito. Recibe los datos del producto
	//Se encarga de introducir los datos en los arrays del objeto carrito
	//luego aumenta en 1 el numero de productos
	function introduce_producto($id_prod,$nombre_prod,$color,$cant_prod,$precio_prod,$llave,$cod_sicapp,$id_suc)
	{
	include("../function.php");	 
		$this->array_id_prod[$this->num_productos]=$id_prod;
		$this->array_nombre_prod[$this->num_productos]=$nombre_prod;
		$this->array_color[$this->num_productos]=$color;
		$this->array_cant_prod[$this->num_productos]=$cant_prod;
		$this->array_precio_prod[$this->num_productos]=$precio_prod;
		$this->array_llave[$this->num_productos]=$llave;
		$this->array_cod_sicapp[$this->num_productos]=$cod_sicapp;
		$this->array_suc[$this->num_productos]=$id_suc;
		$this->num_productos++;
		actualiza_stock($cod_sicapp,$cant_prod,$id_suc);
	}
	
//Muestra el contenido del carrito de la compra
	//ademas pone los enlaces para eliminar un producto del carrito
	function imprime_carrito($id_suc){

			$suma = 0;
		echo '<table width="80%" border="1"  cellpadding="0" cellspacing="0" align=center class="tabla">
			  <tr bgcolor="#000000">
				<td align=center ><font color="#BE0000" face="Arial, Helvetica, sans-serif"><b>Codigo</b></font></td>
				<td align=center ><font color="#BE0000" face="Arial, Helvetica, sans-serif"><b>Nombre Producto</b></font></td>
				<td align=center ><font color="#BE0000" face="Arial, Helvetica, sans-serif"><b>Cantidad</b></font></td>
				<td align=center ><font color="#BE0000" face="Arial, Helvetica, sans-serif"><b>Precio Bs.</b></font></td>
				<td align=center ><font color="#BE0000" face="Arial, Helvetica, sans-serif"><b>Total Bs.</b></font></td>
				<td align=center ><font color="#BE0000" face="Arial, Helvetica, sans-serif"><b>Eliminar</b></font></td>
			  </tr>';

		for ($i=0;$i<$this->num_productos;$i++){
			 // echo"if($this->num_productos)";
			if($this->array_llave[$i]!=0){
				echo "<tr onMouseOver=\"mOvr(this,'#BBE1E1');\" onMouseOut=\"mOut(this,'');\">";
				echo "<td>" . $this->array_id_prod[$i] . "</td>";
				echo "<td>" . $this->array_nombre_prod[$i] ."  ". $this->array_color[$i] . "</td>";
				echo "<td align=center>" . $this->array_cant_prod[$i] . "</td>";
				echo "<td align=right>" . number_format($this->array_precio_prod[$i],2,'.',',') . "</td>";
				$subtotal=$this->array_cant_prod[$i] * $this->array_precio_prod[$i];
				echo "<td align=right>" . number_format($subtotal,2,'.',',') . "</td>";
				echo "<td align=center><a href='eliminar_producto.php?linea=$i&id_suc=$id_suc'><img src=\"imgs/eliminar.jpg\" width=\"15\" height=\"15\" border='0'/></td>";
				echo '</tr>';
				$suma += $this->array_cant_prod[$i] * $this->array_precio_prod[$i];
			}
		}
		//muestro el total
		echo "<tr><td colspan='4'><b>TOTAL:</b></td><td align=right> <b>" .number_format($suma,2,'.',','). "</b></td><td>&nbsp;</td></tr>";
		//total m�s IVA
		//echo "<tr><td><b>IVA (16%):</b></td><td>&nbsp;</td><td>&nbsp;</td><td align=center> <b>" . $suma * 1.16 . "</b></td><td>&nbsp;</td></tr>";
		echo "</table>";
	}
	//elimina un producto del carrito. recibe la linea del carrito que debe eliminar
	//no lo elimina realmente, simplemente pone a cero el id, para saber que esta en estado retirado
	function elimina_producto($linea,$id_suc){
	include("../function.php");
		$this->array_llave[$linea]=0;
		$id=$this->array_cod_sicapp[$linea];
		$cant=$this->array_cant_prod[$linea];
		restaura_stock($id,$cant,$id_suc);
	}
// funcion que cuenta el numero de filas que debe teber la factura
 function control_filas()
   { $cont=0; 
   for ($i=0;$i<$this->num_productos;$i++)
         {
			if($this->array_llave[$i]!=0)
				$cont++;
		 }
  return $cont;		 
   } 
   // Funcion para controlar duplicados
function duplicados($articulo)
 {  $cant=0; $id_prod="$articulo"; //echo "**$articulo**<br>";
  for ($i=0;$i<$this->num_productos;$i++)
    {  $aux=$this->array_cod_sicapp[$i]; 
		$elim=$this->array_llave[$i];
	//	echo $aux;
		// echo "****<br>";
		//$aux="$aux";
    //echo "$aux==$id_prod"; echo ",";
    if($aux==$id_prod && $elim!= 0)
     { $cant++; }
    }  
  return $cant;
 }

   
////// insertar datos a la tabla
function insertar_bd($id_trasp,$origen,$recibe,$fecha)
	{
	include("../conex.php");
	include("../function.php");
    $link=conectarse(); 
	$nombre_usr=$_SESSION["nombre_usr"];
	$fecha_act=date('Y-m-d');
	$hora=date("H:i:s");
	$id_trasp=1; 
	
	
//	$result=mysql_query("select id_trasp from traspaso where anulado='si' and reemitido='no' order by id_trasp asc",$link); where (anulado='no' and reemitido='no') || (anulado='si' and reemitido='si') 
	$result1=mysql_query("select max(id_trasp) from traspaso ",$link);
  /*   if(mysql_num_rows($result)!=0)
	   { $suma_t=0;
	     while($row=mysql_fetch_array($result)) 
	       { $id_trasp=$row[0]; break; } 
			
		   mysql_query("delete from detalle_traspaso where id_trasp='$id_trasp'",$link);
		   
		    for ($i=0;$i<$this->num_productos;$i++)
		  { $suma=0;
			if($this->array_llave[$i]!=0)
			{ $id=$this->array_id_prod[$i];
			  $codigo=$this->array_cod_sicapp[$i];
			  $llave=$this->array_llave[$i];
			  $cant=$this->array_cant_prod[$i];
			  $precio=$this->array_precio_prod[$i];
			  $suma += $this->array_cant_prod[$i] * $this->array_precio_prod[$i];
			  $suma_t+=$suma;
              $id_p=$codigo;
              $canti_actual=cantidad_almacen($id_trasp,$codigo,$cant,$recibe); 
			  mysql_query("insert into detalle_traspaso(id_trasp,id_prod,cantidad,costo_u,costo_t) values('$id_trasp','$id_p','$cant','$precio','$suma')",$link);
			  $result9=mysql_query("select id_prod from almacen where id_prod='$codigo' and id_sucursal='$recibe'",$link);
			 if(mysql_num_rows($result9)!=0)
			   { 
	    		 mysql_query("update almacen set cantidad='$canti_actual',fecha_trasp='$fecha' where id_prod='$codigo' and id_sucursal='$recibe'",$link);
			    } 
			else
			   { mysql_query("insert into almacen(indice,id_prod,fecha_trasp,id_sucursal,cantidad) values('','$id_p','$fecha','$recibe','$cant')",$link);}
		
		//	 actualiza_almacen($id_p,$cant,$recibe,$fecha);
		// mysql_query("update almacen set(id_trasp,id_prod,cantidad,costo_u,costo_t) values('$id_trasp','$id_p','$cant','$precio','$suma')",$link);
			  }
		}
	//	 mysql_query("insert into traspaso(id_trasp,envia, recibe, fecha, anulado, reemitido) values ('$id_trasp','$envia','$recibe','$fecha','si','no') ",$link);
		 mysql_query("update traspaso set reemitido='si',fecha='$fecha' where id_trasp='$id_trasp'",$link);
	   } 
	   //cuando son registros nuevos//////////////////////////////////
	  else*/
	    {  while($row1=mysql_fetch_array($result1)) 
		   $id_trasp=$row1[0]+1; 
		   $suma_t=0;
		    for ($i=0;$i<$this->num_productos;$i++)
		  { $suma=0;
			if($this->array_llave[$i]!=0)
			{ $id=$this->array_id_prod[$i];
			$codigo=$this->array_cod_sicapp[$i];
			  $llave=$this->array_llave[$i];
			  $cant=$this->array_cant_prod[$i];
			  $precio=$this->array_precio_prod[$i];
			  $suma += $this->array_cant_prod[$i] * $this->array_precio_prod[$i];
			  $suma_t+=$suma;
			  $id_p=$codigo;
			  $canti_actual=cantidad_almacen($id_trasp,$codigo,$cant,$recibe);
			//  echo"++++++++++++++++++++++$canti_actual+++++++++++++++++++++++";
			$consulta_detalle="insertar-detalle-traspaso,$id_trasp,$id_p,$cant,$precio,$suma";
			   mysql_query("insert into detalle_traspaso(id_trasp,id_prod,cantidad,costo_u,costo_t) values('$id_trasp','$id_p','$cant','$precio','$suma')",$link);
			   mysql_query("insert into control (indice,accion,usuario,fecha,hora) values ('','$consulta_detalle','$nombre_usr','$fecha_act','$hora')",$link);
				//echo "select id_prod from almacen where id_prod='$codigo' and id_sucursal='$recibe'";
			 $result9=mysql_query("select id_prod from almacen where id_prod='$codigo' and id_sucursal='$recibe'",$link);
			 if(mysql_num_rows($result9)!=0)
			   { //echo"update almacen set cantidad='$canti_actual',fecha_trasp='$fecha' where id_prod='$codigo' and id_sucursal='$recibe'";
	    		 $consulta_almacen="actualiza-almacen cantidad=$canti_actual,fecha_trasp=$fecha";
				 mysql_query("update almacen set cantidad='$canti_actual',fecha_trasp='$fecha' where id_prod='$codigo' and id_sucursal='$recibe'",$link);
				 mysql_query("insert into control (indice,accion,usuario,fecha,hora) values ('','$consulta_almacen','$nombre_usr','$fecha_act','$hora')",$link);
			    } 
			else
			   { 
			   $consulta_insertar="insertar-almacen $id_p,$fecha,$recibe,$cant";
			   mysql_query("insert into almacen(indice,id_prod,fecha_trasp,id_sucursal,cantidad) values('','$id_p','$fecha','$recibe','$cant')",$link);
			   mysql_query("insert into control (indice,accion,usuario,fecha,hora) values ('','$consulta_insertar','$nombre_usr','$fecha_act','$hora')",$link);
			   }
			}
          }
		 // $monto_bol=$suma_t; //*$cambio;
				$consulta_traspaso="insertar-traspaso $id_trasp,$origen,$recibe,no,no,$fecha";
			  mysql_query("insert into traspaso(id_trasp,envia,recibe,anulado,reemitido,fecha)
			              values('$id_trasp','$origen','$recibe','no','no','$fecha') ",$link);
						   mysql_query("insert into control (indice,accion,usuario,fecha,hora) values ('','$consulta_traspaso','$nombre_usr','$fecha_act','$hora')",$link);
						  
			 }	

return $id_trasp;	                
}
///////////fin insertar

}//fin de la clase
///////////
//inicio la sesi�n
session_start();
//si no esta creado el objeto carrito en la sesion, lo creo
if (!isset($_SESSION["ocarrito"])){
	$_SESSION["ocarrito"] = new carrito();
}
?> 
<html> 
<head>
<link href="../../general.css" rel="stylesheet" type="text/css" />
</head>
<body> 
</body>
</html>