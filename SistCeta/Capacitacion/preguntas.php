<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="es-ES" class="ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="es-ES" class="ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="es-ES" class="ie8"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="es-ES"> <!--<![endif]-->

<!-- BEGIN head -->
<head>

	<!-- Meta Tags -->
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge;chrome=1" >
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	
	<link rel="shortcut icon" href="http://www.infocalcbba.edu.bo/wp-content/uploads/2016/04/favicon.ico" type="image/x-icon" />	
	<!-- Title -->
	<title>Formularios de Interes | Fundacion Infocal Cochabamba</title>
	
	<!-- RSS Feeds & Pingbacks -->
	<link rel="alternate" type="application/rss+xml" title="Fundacion Infocal Cochabamba RSS Feed" href="http://www.infocalcbba.edu.bo/feed/" />
	<link rel="pingback" href="http://www.infocalcbba.edu.bo/xmlrpc.php" />
	
	<style type="text/css">
	
	h1, h2, h3, h4, h5, h6, .slides .flex-caption p, .page-content blockquote, .event-full .event-info h4, .page-content table th, #cancel-comment-reply-link {
	font-family: 'Merriweather', serif !important;}body {
			background: #ffffff !important;
		}#footer-wrapper {
			background: #172557 !important;
		}#footer-bottom, #footer .tagcloud a, #footer .widget-title-block, .course-finder-full-form {
			background: #172557 !important;
		}#footer #twitter_update_list li a, #footer-bottom p, #footer .tp_recent_tweets .twitter_time, #footer .tp_recent_tweets a {
			color: #ffffff !important;
		}.content-wrapper {
			margin: 0 auto 0 auto !important;
		}.slider .slides li {
			max-height: 830px;
		}.loading .slide-loader {
	 		min-height: 830px;
		}#header-wrapper {
		 	position: absolute;
		}</style>	
	<link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,700,900' rel='stylesheet' type='text/css'><link rel="alternate" type="application/rss+xml" title="Fundacion Infocal Cochabamba &raquo; Feed" href="http://www.infocalcbba.edu.bo/feed/" />
<link rel="alternate" type="application/rss+xml" title="Fundacion Infocal Cochabamba &raquo; RSS de los comentarios" href="http://www.infocalcbba.edu.bo/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Fundacion Infocal Cochabamba &raquo; Formularios de Interes RSS de los comentarios" href="http://www.infocalcbba.edu.bo/formularios-de-interes/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/www.infocalcbba.edu.bo\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.3.4"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='jquery.prettyphoto-css'  href='http://www.infocalcbba.edu.bo/wp-content/plugins/wp-video-lightbox/css/prettyPhoto.css?ver=4.3.4' type='text/css' media='all' />
<link rel='stylesheet' id='video-lightbox-css'  href='http://www.infocalcbba.edu.bo/wp-content/plugins/wp-video-lightbox/wp-video-lightbox.css?ver=4.3.4' type='text/css' media='all' />
<link rel='stylesheet' id='siteorigin-panels-front-css'  href='http://www.infocalcbba.edu.bo/wp-content/plugins/siteorigin-panels/css/front.css?ver=2.4.8' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://www.infocalcbba.edu.bo/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='foobox-min-css'  href='http://www.infocalcbba.edu.bo/wp-content/plugins/fooboxV2/css/foobox.min.css?ver=2.3.0.4.1' type='text/css' media='all' />
<link rel='stylesheet' id='sb_instagram_styles-css'  href='http://www.infocalcbba.edu.bo/wp-content/plugins/instagram-feed/css/sb-instagram.min.css?ver=1.4.3' type='text/css' media='all' />
<link rel='stylesheet' id='sb_instagram_icons-css'  href='https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css?ver=4.2.0' type='text/css' media='all' />
<link rel='stylesheet' id='mappress-css'  href='http://www.infocalcbba.edu.bo/wp-content/plugins/mappress-google-maps-for-wordpress/css/mappress.css?ver=2.43.4' type='text/css' media='all' />
<link rel='stylesheet' id='social_hashtag-style-css'  href='http://www.infocalcbba.edu.bo/wp-content/plugins/social-hashtags/lib/social_hashtag.css?ver=4.3.4' type='text/css' media='all' />
<link rel='stylesheet' id='parkcollege-style-css'  href='http://www.infocalcbba.edu.bo/wp-content/themes/parkcollege/style.css?ver=4.3.4' type='text/css' media='all' />
<link rel='stylesheet' id='superfish-css'  href='http://www.infocalcbba.edu.bo/wp-content/themes/parkcollege/css/superfish.css?ver=4.3.4' type='text/css' media='all' />
<link rel='stylesheet' id='prettyPhoto-css'  href='http://www.infocalcbba.edu.bo/wp-content/themes/parkcollege/css/prettyPhoto.css?ver=4.3.4' type='text/css' media='all' />
<link rel='stylesheet' id='flexslider-css'  href='http://www.infocalcbba.edu.bo/wp-content/themes/parkcollege/css/flexslider.css?ver=4.3.4' type='text/css' media='all' />
<link rel='stylesheet' id='responsive-css'  href='http://www.infocalcbba.edu.bo/wp-content/themes/parkcollege/css/responsive.css?ver=4.3.4' type='text/css' media='all' />
<link rel='stylesheet' id='colour-css'  href='http://www.infocalcbba.edu.bo/wp-content/themes/parkcollege/css/colour.css?ver=4.3.4' type='text/css' media='all' />
<link rel='stylesheet' id='qns-opensans-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C300italic%2C400italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic&#038;ver=4.3.4' type='text/css' media='all' />
<link rel='stylesheet' id='tablepress-default-css'  href='http://www.infocalcbba.edu.bo/wp-content/plugins/tablepress/css/default.min.css?ver=1.7' type='text/css' media='all' />
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-includes/js/jquery/jquery.js?ver=1.11.3'></script>
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-content/plugins/wp-video-lightbox/js/jquery.prettyPhoto.js?ver=3.1.6'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var vlpp_vars = {"prettyPhoto_rel":"wp-video-lightbox","animation_speed":"fast","slideshow":"5000","autoplay_slideshow":"false","opacity":"0.80","show_title":"true","allow_resize":"true","allow_expand":"true","default_width":"640","default_height":"480","counter_separator_label":"\/","theme":"pp_default","horizontal_padding":"20","hideflash":"false","wmode":"opaque","autoplay":"false","modal":"false","deeplinking":"false","overlay_gallery":"true","overlay_gallery_max":"30","keyboard_shortcuts":"true","ie6_fallback":"true"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-content/plugins/wp-video-lightbox/js/video-lightbox.js?ver=3.1.6'></script>
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-content/plugins/fooboxV2/js/foobox.min.js?ver=2.3.0.4.1'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.infocalcbba.edu.bo/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.infocalcbba.edu.bo/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.3.4" />
<link rel='canonical' href='http://www.infocalcbba.edu.bo/formularios-de-interes/' />
<link rel='shortlink' href='http://www.infocalcbba.edu.bo/?p=608' />
<script type="text/javascript">/* Run FooBox (v2.3.0.4.1) */
(function( FOOBOX, $, undefined ) {
  FOOBOX.o = {wordpress: { enabled: true }, deeplinking : { enabled: true, prefix: "foobox" }, countMessage:'item %index of %total', excludes:'.fbx-link,.nofoobox,a[href*="pinterest.com/pin/create/button/"]', affiliate : { enabled: true }, slideshow: { enabled:true}, social: { enabled: false }, preload:true};
  FOOBOX.init = function() {
    $(".fbx-link").removeClass("fbx-link");
    $(".gallery").foobox(FOOBOX.o);
    $(".foobox, [target=\"foobox\"]").foobox(FOOBOX.o);
    $(".wp-caption").foobox(FOOBOX.o);
    $("a:has(img[class*=wp-image-])").foobox(FOOBOX.o);

  };
}( window.FOOBOX = window.FOOBOX || {}, jQuery ));

jQuery(function() {
  //preload the foobox font
  jQuery("body").append("<span style=\"font-family:'foobox'; color:transparent; position:absolute; top:-1000em;\">f</span>");
  FOOBOX.init();
  jQuery(document).trigger("foobox-after-init");

});
</script><link rel="stylesheet" href="http://www.infocalcbba.edu.bo/wp-content/plugins/slimbox-plugin/slimbox/slimbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="http://www.infocalcbba.edu.bo/wp-content/plugins/slimbox-plugin/slimbox/mootools.x.js"></script>
<script type="text/javascript" src="http://www.infocalcbba.edu.bo/wp-content/plugins/slimbox-plugin/slimbox/slimbox.js"></script>
<style type="text/css">

</style>	
<!-- END head -->
</head>

<!-- BEGIN body -->
<body id="top" class="page page-id-608 page-template-default loading">

	<!-- BEGIN #header-wrapper -->
<div id="header-wrapper">
		
		<!-- BEGIN #header-border -->
		<div id="header-border">
		
			<!-- BEGIN #header-top -->
			<div id="header-top" class="clearfix">

				<ul class="top-left-nav clearfix"><li id="menu-item-23" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-23"><a target="_blank" href="http://www.infocalcbba.edu.bo/webmail">CORREO</a><span>/</span></li>
<li id="menu-item-24" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-24"><a target="_blank" href="http://www.infocalcbba.edu.bo/moodle">CAMPUS VIRTUAL</a><span>/</span></li>
<li id="menu-item-26" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-26"><a target="_blank" href="http://www.infocalcbba.edu.bo/medios">MEDIOS</a><span>/</span></li>
</ul>				
								<ul class="top-right-nav clearfix">
					
										<li class="phone-icon">591-4-4242660 / 591-4-4716777</li>
										
										<li class="email-icon">webmaster@infocalcbba.edu.bo</li>
									
				</ul>
							
			<!-- END #header-top -->
			</div>
			
			<!-- BEGIN #header-content-wrapper -->
			<div id="header-content-wrapper" class="clearfix">

									<div id="logo" class="site-title-image">
						<h1>
							<a href="http://www.infocalcbba.edu.bo"><img src="http://www.infocalcbba.edu.bo/wp-content/uploads/2015/04/Sin-título-1-011.png" alt="" /></a>
						</h1>
					</div>
				
				
				<ul class="social-icons clearfix"><li><a href="https://twitter.com/CbbaInfocal" target="_blank"><span class="twitter-icon"></span></a></li><li><a href="https://www.facebook.com/pages/Infocal-Cochabamba-Oficial/203468493005278?ref=ts&fref=ts" target="_blank"><span class="facebook-icon"></span></a></li><li><a href="https://instagram.com/infocalcbba/" target="_blank"><span class="pinterest-icon"></span></a></li><li><a href="https://www.youtube.com/channel/UCXelupMXw3d1rQTpZk4bqvw" target="_blank"><span class="youtube-icon"></span></a></li></ul>			
			<!-- END #header-content-wrapper -->
			</div>
		
			<!-- BEGIN #main-menu-wrapper -->
		  <div id="main-menu-wrapper" class="clearfix">
				
			  <ul id="main-menu"><li id="menu-item-4" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-4"><a href="http://www.infocalcbba.edu.bo/">Inicio</a></li>
<li id="menu-item-16" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-16"><a href="#">Nosotros</a>
<ul class="sub-menu">
	<li id="menu-item-199" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-199"><a href="http://www.infocalcbba.edu.bo/quienes-somos/">¿Quienes Somos?</a></li>
	<li id="menu-item-710" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-710"><a href="http://www.infocalcbba.edu.bo/directorio-infocal-cochabamba/">Directorio Infocal Cochabamba</a></li>
	<li id="menu-item-2705" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2705"><a href="http://www.infocalcbba.edu.bo/plantel-administrativo/">Plantel Ejecutivo</a></li>
	<li id="menu-item-2704" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2704"><a href="#">Jefes de Carrera</a>
	<ul class="sub-menu">
		<li id="menu-item-2709" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2709"><a href="http://www.infocalcbba.edu.bo/jc-tupuraya/">JC Tupuraya</a></li>
		<li id="menu-item-2706" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2706"><a href="http://www.infocalcbba.edu.bo/jc-arocagua/">JC Arocagua</a></li>
	</ul>
</li>
	<li id="menu-item-2962" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2962"><a href="http://www.infocalcbba.edu.bo/bienestar-estudiantil-e-institucional/">Bienestar Estudiantil e Institucional</a></li>
	<li id="menu-item-197" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-197"><a href="http://www.infocalcbba.edu.bo/convenios/">Convenios</a></li>
</ul>
</li>
<li id="menu-item-2629" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2629"><a href="#">Formación</a>
<ul class="sub-menu">
	<li id="menu-item-17" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-17"><a href="#">Carreras</a>
	<ul class="sub-menu">
		<li id="menu-item-75" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-75"><a href="http://www.infocalcbba.edu.bo/mecanica-automotriz/">Mecánica Automotriz</a></li>
		<li id="menu-item-82" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-82"><a href="http://www.infocalcbba.edu.bo/electrotecnia-industrial/">Electrotécnia Industrial</a></li>
		<li id="menu-item-81" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-81"><a href="http://www.infocalcbba.edu.bo/mecanica-industrial/">Mecánica Industrial</a></li>
		<li id="menu-item-83" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-83"><a href="http://www.infocalcbba.edu.bo/gastronomia/">Gastronomía</a></li>
		<li id="menu-item-80" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-80"><a href="http://www.infocalcbba.edu.bo/parvulario/">Parvulario</a></li>
		<li id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a href="http://www.infocalcbba.edu.bo/instalaciones-integrales-y-de-gas/">Instalaciones Integrales y de Gas</a></li>
		<li id="menu-item-77" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77"><a href="http://www.infocalcbba.edu.bo/electromecanica-industrial/">Electromecánica Industrial</a></li>
		<li id="menu-item-76" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-76"><a href="http://www.infocalcbba.edu.bo/sistemas-informaticos/">Sistemas Informáticos</a></li>
		<li id="menu-item-3045" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3045"><a href="http://www.infocalcbba.edu.bo/turismo-hoteleria/">Turismo</a></li>
	</ul>
</li>
	<li id="menu-item-140" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-140"><a href="http://www.infocalcbba.edu.bo/reglamentos/">Reglamentos</a></li>
	<li id="menu-item-2667" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2667"><a href="http://www.infocalcbba.edu.bo/calendario-academico/">Calendario Académico</a></li>
</ul>
</li>
<li id="menu-item-18" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-18"><a href="#">Capacitación</a>
<ul class="sub-menu">
	<li id="menu-item-2711" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2711"><a href="http://www.infocalcbba.edu.bo/cronograma-capacitacion/">Cronograma Capacitación</a></li>
	<li id="menu-item-610" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-608 current_page_item menu-item-610"><a href="http://www.infocalcbba.edu.bo/formularios-de-interes/">Formularios de Interes</a></li>
</ul>
</li>
<li id="menu-item-19" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-19"><a href="#">Admisiones</a>
<ul class="sub-menu">
	<li id="menu-item-192" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-192"><a href="http://www.infocalcbba.edu.bo/requisitos-de-inscripcion/">Requisitos de Inscripción</a></li>
</ul>
</li>
<li id="menu-item-230" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-230"><a href="#">Contáctanos</a>
<ul class="sub-menu">
	<li id="menu-item-226" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-226"><a href="http://www.infocalcbba.edu.bo/contacto/">Contacto Tupuraya</a></li>
	<li id="menu-item-231" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-231"><a href="http://www.infocalcbba.edu.bo/contacto-arocagua/">Contacto Arocagua</a></li>
</ul>
</li>
</ul>				
								
				<div class="menu-search-button"></div>
				<form method="get" action="http://www.infocalcbba.edu.bo/" class="menu-search-form">
					<input class="menu-search-field" type="text" onblur="if(this.value=='')this.value='Escribe y presiona enter';" onfocus="if(this.value=='Escribe y presiona enter')this.value='';" value="Escribe y presiona enter" name="s" />
				</form>
				
						
			<!-- END #main-menu-wrapper -->
			</div>
		
		<!-- END #header-border -->
		</div>
	
	<!-- END #header-wrapper -->
	</div>
<!-- BEGIN .page-header -->
<div class="page-header clearfix" style="background:url(http://www.infocalcbba.edu.bo/wp-content/uploads/2015/04/23_demo_image.jpg);">
	
	<div class="page-header-inner clearfix">	
		<div class="page-title">	
			<h2>Formularios de Interes</h2>
			<div class="page-title-block"></div>
		</div>
		<div class="breadcrumbs clearfix"><p> <a rel="v:url" property="v:title" href="http://www.infocalcbba.edu.bo">Home</a> &raquo; Formularios de Interes </p></div>	</div>
	
<!-- END .page-header -->
</div>

<!-- BEGIN .content-wrapper -->
<div class="content-wrapper page-content-wrapper clearfix">

	<!-- BEGIN .main-content -->
	<div class="main-content page-content">
	
		<!-- BEGIN .inner-content-wrapper -->
		<div class="inner-content-wrapper">

					<table border="0" width="100%" cellspacing="8" cellpadding="0">
<tbody>
<tr>
<td>
<p style="text-align: justify;">Los Formularios de Interés en participación de cursos de capacitación tiene como objetivo recolectar la necesidad para capacitarse del publico en general para de esta manera planificar el lanzamientos de cursos efectivos.</p>
<p style="text-align: justify;">Realice el envío de su solicitud para nuestra base de datos y luego nos contactaremos con usted en cuanto el curso ya este programado y listo para comenzar con su proceso de inscripción efectiva.</p>
</td>
</tr>
<tr>
<th style="text-align: left;"><strong><span style="color: #003366;">Formularios:</span> </strong></th>
</tr>
<tr>
<td>
<ul>
<li>Electricidad y Electrónica Automotriz</li>
<li>Mecánica Automotriz</li>
</ul>
</td>
</tr>
<tr>
<th style="text-align: left;"></th>
</tr>
</tbody>
</table>
			
						
			
<!-- BEGIN #respond-wrapper -->
<div id="respond-wrapper">


	<h3 id="comment-number" class="block-title">6 Comments</h3>

	<ul class="comments">
				
	<li class="comment even thread-even depth-1 comment-entry clearfix" id="comment-3762">

		<!-- BEGIN .comment-left -->
		<div class="comment-left">
			<div class="comment-image">
				<img alt='' src='http://0.gravatar.com/avatar/61149c8c034e52e4acc637285452e54a?s=60&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/61149c8c034e52e4acc637285452e54a?s=120&amp;d=mm&amp;r=g 2x' class='avatar avatar-60 photo' height='60' width='60' />			</div>
		<!-- END .comment-left -->
		</div>

		<!-- BEGIN .comment-right -->
		<div class="comment-right">
					
			<p class="comment-info">oliver zambrana 
				<span><a href="http://www.infocalcbba.edu.bo/formularios-de-interes/#comment-3762">
				3 agosto, 2015 at 0:45				</a></span>
			</p>
					
			<div class="comment-text">
								<p>&nbsp;</p>
			</div>
					
			<p><span class="reply">
				<a rel='nofollow' class='comment-reply-link' href='http://www.infocalcbba.edu.bo/formularios-de-interes/?replytocom=3762#respond' onclick='return addComment.moveForm( "comment-3762", "3762", "respond", "608" )' aria-label='Responder a oliver zambrana'>Responder</a>							</span></p>

		<!-- END .comment-right -->
		</div>		

	<ul class="children">
		
	<li class="comment byuser comment-author-astambuk odd alt depth-2 comment-entry clearfix" id="comment-3764">

		<!-- BEGIN .comment-left -->
		<div class="comment-left">
			<div class="comment-image">
				<img alt='' src='http://1.gravatar.com/avatar/1b529cb50dc5ccdbb3adae957737add3?s=60&#038;d=mm&#038;r=g' srcset='http://1.gravatar.com/avatar/1b529cb50dc5ccdbb3adae957737add3?s=120&amp;d=mm&amp;r=g 2x' class='avatar avatar-60 photo' height='60' width='60' />			</div>
		<!-- END .comment-left -->
		</div>

		<!-- BEGIN .comment-right -->
		<div class="comment-right">
					
			<p class="comment-info">Andrea Stambuk 
				<span><a href="http://www.infocalcbba.edu.bo/formularios-de-interes/#comment-3764">
				3 agosto, 2015 at 12:57				</a></span>
			</p>
			<p><span class="reply">
			  <a rel='nofollow' class='comment-reply-link' href='http://www.infocalcbba.edu.bo/formularios-de-interes/?replytocom=3764#respond' onclick='return addComment.moveForm( "comment-3764", "3764", "respond", "608" )' aria-label='Responder a Andrea Stambuk'>Responder</a>							</span></p>

		<!-- END .comment-right -->
		</div>		

	</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
		
	<li class="comment even thread-odd thread-alt depth-1 comment-entry clearfix" id="comment-3844">

		<!-- BEGIN .comment-left -->
		<div class="comment-left">
			<div class="comment-image">
				<img alt='' src='http://1.gravatar.com/avatar/a63b7dfa4c92fb0d7de93f37ad4d6291?s=60&#038;d=mm&#038;r=g' srcset='http://1.gravatar.com/avatar/a63b7dfa4c92fb0d7de93f37ad4d6291?s=120&amp;d=mm&amp;r=g 2x' class='avatar avatar-60 photo' height='60' width='60' />			</div>
		<!-- END .comment-left -->
		</div>

		<!-- BEGIN .comment-right -->
		<div class="comment-right">
					
			<p class="comment-info">Wilmer 
				<span><a href="http://www.infocalcbba.edu.bo/formularios-de-interes/#comment-3844">
				28 agosto, 2015 at 23:32				</a></span>
			</p>
					
			<div class="comment-text">
								<p>&nbsp;</p>
			</div>
					
			<p><span class="reply">
				<a rel='nofollow' class='comment-reply-link' href='http://www.infocalcbba.edu.bo/formularios-de-interes/?replytocom=3844#respond' onclick='return addComment.moveForm( "comment-3844", "3844", "respond", "608" )' aria-label='Responder a Wilmer'>Responder</a>							</span></p>

		<!-- END .comment-right -->
		</div>		

	<ul class="children">
		
	<li class="comment byuser comment-author-astambuk odd alt depth-2 comment-entry clearfix" id="comment-3888">

		<!-- BEGIN .comment-left -->
		<div class="comment-left">
			<div class="comment-image">
				<img alt='' src='http://1.gravatar.com/avatar/1b529cb50dc5ccdbb3adae957737add3?s=60&#038;d=mm&#038;r=g' srcset='http://1.gravatar.com/avatar/1b529cb50dc5ccdbb3adae957737add3?s=120&amp;d=mm&amp;r=g 2x' class='avatar avatar-60 photo' height='60' width='60' />			</div>
		<!-- END .comment-left -->
		</div>

		<!-- BEGIN .comment-right -->
		<div class="comment-right">
					
			<p class="comment-info">Andrea Stambuk 
				<span><a href="http://www.infocalcbba.edu.bo/formularios-de-interes/#comment-3888">
				2 septiembre, 2015 at 14:11				</a></span>
			</p>
					
			<div class="comment-text">
								<p>&nbsp;</p>
			</div>
					
			<p><span class="reply">
				<a rel='nofollow' class='comment-reply-link' href='http://www.infocalcbba.edu.bo/formularios-de-interes/?replytocom=3888#respond' onclick='return addComment.moveForm( "comment-3888", "3888", "respond", "608" )' aria-label='Responder a Andrea Stambuk'>Responder</a>							</span></p>

		<!-- END .comment-right -->
		</div>		

	</li><!-- #comment-## -->
</ul><!-- .children -->
</li><!-- #comment-## -->
		
	<li class="comment even thread-even depth-1 comment-entry clearfix" id="comment-3768">

		<!-- BEGIN .comment-left -->
		<div class="comment-left">
			<div class="comment-image">
				<img alt='' src='http://2.gravatar.com/avatar/2f905eae7931eac81ace08c36b9e03c9?s=60&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/2f905eae7931eac81ace08c36b9e03c9?s=120&amp;d=mm&amp;r=g 2x' class='avatar avatar-60 photo' height='60' width='60' />			</div>
		<!-- END .comment-left -->
		</div>

		<!-- BEGIN .comment-right -->
		<div class="comment-right">
					
			<p class="comment-info">Freddy Gutierrez 
				<span><a href="http://www.infocalcbba.edu.bo/formularios-de-interes/#comment-3768">
				4 agosto, 2015 at 21:11				</a></span>
			</p>
					
			<div class="comment-text">
								<p>&nbsp;</p>
			</div>
					
			<p><span class="reply">
				<a rel='nofollow' class='comment-reply-link' href='http://www.infocalcbba.edu.bo/formularios-de-interes/?replytocom=3768#respond' onclick='return addComment.moveForm( "comment-3768", "3768", "respond", "608" )' aria-label='Responder a Freddy Gutierrez'>Responder</a>							</span></p>

		<!-- END .comment-right -->
		</div>		

	</li><!-- #comment-## -->
		
	<li class="comment byuser comment-author-astambuk odd alt thread-odd thread-alt depth-1 comment-entry clearfix" id="comment-3771">

		<!-- BEGIN .comment-left -->
		<div class="comment-left">
			<div class="comment-image">
				<img alt='' src='http://1.gravatar.com/avatar/1b529cb50dc5ccdbb3adae957737add3?s=60&#038;d=mm&#038;r=g' srcset='http://1.gravatar.com/avatar/1b529cb50dc5ccdbb3adae957737add3?s=120&amp;d=mm&amp;r=g 2x' class='avatar avatar-60 photo' height='60' width='60' />			</div>
		<!-- END .comment-left -->
		</div>

		<!-- BEGIN .comment-right -->
		<div class="comment-right">
					
			<p class="comment-info">Andrea Stambuk 
				<span><a href="http://www.infocalcbba.edu.bo/formularios-de-interes/#comment-3771">
				5 agosto, 2015 at 18:41				</a></span>
			</p>
					
			<div class="comment-text">
								<p>&nbsp;</p>
			</div>
					
			<p><span class="reply">
				<a rel='nofollow' class='comment-reply-link' href='http://www.infocalcbba.edu.bo/formularios-de-interes/?replytocom=3771#respond' onclick='return addComment.moveForm( "comment-3771", "3771", "respond", "608" )' aria-label='Responder a Andrea Stambuk'>Responder</a>							</span></p>

		<!-- END .comment-right -->
		</div>		

	</li><!-- #comment-## -->
	</ul>

	
						<div id="respond" class="comment-respond">
				<h3 id="reply-title" class="comment-reply-title">Dejar un comentario <small><a rel="nofollow" id="cancel-comment-reply-link" href="/formularios-de-interes/#respond" style="display:none;">Cancelar replica al comentario</a></small></h3>
									<form action="http://www.infocalcbba.edu.bo/wp-comments-post.php" method="post" id="commentform" class="comment-form">
																										<label>Name <span class="required">(required)</span></label><input id="author" class="text_input" name="author" type="text" value="" placeholder="Name"  />
<label>Email <span class="required">(required)</span></label><input id="email" class="text_input" name="email" type="text" value="" placeholder="Email"  />
<label>Website </label><input id="url" class="text_input" name="url" type="text" value="" placeholder="Website" />
												<label for="comment">Comentario</label><textarea name="comment" id="comment" class="text_input" tabindex="4" rows="9" cols="60"></textarea>						
						<p class="form-submit"><input name="submit" type="submit" id="submit" class="submit" value="Enviar Comentario" /> <input type='hidden' name='comment_post_ID' value='608' id='comment_post_ID' />
<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
</p><p style="display: none;"><input type="hidden" id="akismet_comment_nonce" name="akismet_comment_nonce" value="45ed52c27d" /></p><p style="display: none;"><input type="hidden" id="ak_js" name="ak_js" value="87"/></p>		<!-- Checkbox by Newsletter Sign-Up Checkbox v2.0.5 - https://wordpress.org/plugins/newsletter-sign-up/ -->
		<p id="nsu-checkbox">
			<label for="nsu-checkbox-input" id="nsu-checkbox-label">
				<input value="1" id="nsu-checkbox-input" type="checkbox" name="newsletter-sign-up-do"  />
				Sign me up for the newsletter			</label>
		</p>
		<!-- / Newsletter Sign-Up -->
							</form>
		</div><!-- #respond -->
			

<!-- END #respond-wrapper -->
</div>		
		<!-- END .inner-content-wrapper -->
		</div>
		
	<!-- END .main-content -->
	</div>

	<div class="sidebar-right page-content"><div class="widget content-block"><h3 class="block-title">Convenios Nacionales</h3>		
		
				

		<div class="slider-blocks clearfix"><ul class="slides slide-loader2">
<li>
	<div class="center-images">
		<a target="_blank" href="http://www.upb.edu/"><img src="http://www.infocalcbba.edu.bo/wp-content/uploads/2015/04/upb.jpg" alt="" class="content-img" /></a>
		<a target="_blank" href="http://http://www.ypfb.gob.bo/es/"><img src="http://www.infocalcbba.edu.bo/wp-content/uploads/2015/04/ypfb.jpg" alt="" class="content-img" /></a>
		<a href="https://www.ucbcba.edu.bo/"><img src="http://www.infocalcbba.edu.bo/wp-content/uploads/2015/04/cato.jpg" alt="" class="content-img" /></a>

<a target="_blank" href="https://www.fepc.org.bo/"><img src="http://www.infocalcbba.edu.bo/wp-content/uploads/2015/04/fepc-11.png" alt="" class="content-img" /></a>

<a target="_blank" href="https://http://www.bco.com.bo/"><img src="http://www.infocalcbba.edu.bo/wp-content/uploads/2015/04/fondo.jpg" alt="" class="content-img" /></a>

<a target="_blank" href="https://www.mintrabajo.gob.bo></a><img src="http://www.infocalcbba.edu.bo/wp-content/uploads/2015/04/ministerio.jpg" alt="" class="content-img" /></a>

<a target="_blank" href="https://www.minerasancristobal.com/"><img src="http://www.infocalcbba.edu.bo/wp-content/uploads/2015/04/san.jpg" alt="" class="content-img" /></a>
	</div>
</li>
</ul></div>		
		</div></div>
<!-- END .content-wrapper -->
</div>

									
				<!-- BEGIN #footer-wrapper -->
				<div id="footer-wrapper">

											
						<!-- BEGIN #footer -->
						<div id="footer" class="footer-no-widgets">

					
						<ul class="columns-4 clearfix">
							
					
							
				</ul>
						
			<!-- BEGIN #footer-bottom -->
			<div id="footer-bottom" class="clearfix">
				
									<p class="fl">© Copyright - <a href="#">Fundación Infocal Cochabamba</a><a href="#"></a></p>
								
				<p class="go-up fr">
					<a class="scrollup" href="#top">Arriba</a>
				</p>
	
			<!-- END #footer-bottom -->
			</div>
			
			<!-- END #footer -->
			</div>

		<!-- END #footer-wrapper -->
		</div>

<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-content/plugins/akismet/_inc/form.js?ver=3.1.11'></script>
<link rel='stylesheet' id='animated-css'  href='http://www.infocalcbba.edu.bo/wp-content/plugins/like-box/includes/style/effects.css?ver=4.3.4' type='text/css' media='all' />
<link rel='stylesheet' id='front_end_like_box-css'  href='http://www.infocalcbba.edu.bo/wp-content/plugins/like-box/includes/style/style.css?ver=4.3.4' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='http://www.infocalcbba.edu.bo/wp-includes/css/dashicons.min.css?ver=4.3.4' type='text/css' media='all' />
<link rel='stylesheet' id='thickbox-css'  href='http://www.infocalcbba.edu.bo/wp-includes/js/thickbox/thickbox.css?ver=4.3.4' type='text/css' media='all' />
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"http:\/\/www.infocalcbba.edu.bo\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","recaptchaEmpty":"Por favor, prueba que no eres un robot.","sending":"Enviando..."};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.4.2'></script>
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js?ver=1.8'></script>
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-content/themes/parkcollege/js/tinynav.min.js?ver=1.4.8'></script>
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-content/themes/parkcollege/js/jquery.uniform.js?ver=1.4.8'></script>
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-content/themes/parkcollege/js/superfish.js?ver=1.4.8'></script>
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-content/themes/parkcollege/js/jquery.prettyPhoto.js?ver=1.1.9'></script>
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-content/themes/parkcollege/js/jquery.flexslider-min.js?ver=1.1.9'></script>
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-content/themes/parkcollege/js/scripts.js?ver=1'></script>
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-content/plugins/like-box/includes/javascript/front_end_js.js?ver=4.3.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var thickboxL10n = {"next":"Siguiente >","prev":"< Anterior","image":"Imagen","of":"de","close":"Cerrar","noiframes":"Esta funci\u00f3n requiere de frames insertados. Tienes los iframes desactivados o tu navegador no los soporta.","loadingAnimation":"http:\/\/www.infocalcbba.edu.bo\/wp-includes\/js\/thickbox\/loadingAnimation.gif"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.infocalcbba.edu.bo/wp-includes/js/thickbox/thickbox.js?ver=3.1-20121105'></script>

	<!-- END body -->
</body>

</html>