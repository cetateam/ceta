<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Ejemplo del uso de formularios - aprenderaprogramar.com</title>
<link href="../../../css/estilos.css" rel="stylesheet" type="text/css">
<style>
body {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #BE0000;
	background-image: url(../../imgs/yoga.jpg);
	background-position: center center;

}
</style>
</head>
<body>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td colspan="2">
      <?Php 
$bandera=$_GET[bandera];
echo"$bandera";
?>
      <p align="center">INSCRIPCION DE ESTUDIANTES REGULARES</p>
      <p align="center">(2do, 3ro, 4to, 5to, 6to, 7mo Semestre)</p>
      <h5 align="left">Datos del estudiante </h5>
      <h5 align="left">(*)Datos obligatorios para la inscripci&oacute;n de estudiantes 
        nuevos</h5>
      </td>
  </tr>
  <tr> 
    <td> 
      <form action="http://www.aprenderaprogramar.com" method="get">
        <table width="743" height="502" border="1">
          <tr> 
            <th height="23" scope="row"><label for="Codigo Ceta"> <div align="left">Codigo 
              Ceta: 
              <input type="text" name="Codigo Ceta" id="Codigo Ceta">
              </div></label>
            </th>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <th width="566" height="49" scope="row"><label for="Apellido paterno5"> 
              <div align="left">(*)Apellido paterno: 
                <input type="text" name="Apellido paterno" id="Apellido paterno5">
                </div></label>
              <div align="left"></div></th>
            <td width="161">Fotocopias de documentos presentados</td>
          </tr>
          <tr> 
            <th scope="row"><div align="left"> 
              <label for="Apellido materno">(*)Apellido materno:</label>
              <input type="text" name="Apellido materno" id="Apellido materno">
            </div></th>
            <td><input type="checkbox" name="Carnet de identidad" id="Carnet de identidad"> 
              <label for="Carnet de identidad">Carnet de identidad</label></td>
          </tr>
          <tr> 
            <th scope="row"><div align="left"> 
              <label for="Nombres">(*)Nombres:</label>
              <input type="text" name="Nombres" id="Nombres">
            </div></th>
            <td><input type="checkbox" name="Titulo de bachiller" id="Titulo de bachiller"> 
              <label for="Titulo de bachiller">Titulo de bachiller</label></td>
          </tr>
          <tr> 
            <th scope="row"><div align="left"> 
              <label for="Fecha nacimineto ">(*)Fecha nacimineto :</label>
              <select name="Fecha nacimineto " id="Fecha nacimineto ">
                </select>
              <label for="Estado Civil">Estado Civil:</label>
              <select name="Estado Civil" id="Estado Civil">
                </select>
            </div></th>
            <td><input type="checkbox" name="Certificado de nacimiento" id="Certificado de nacimiento"> 
              <label for="Certificado de nacimiento">Certificado de nacimiento</label></td>
          </tr>
          <tr> 
            <th scope="row"><div align="left">(*)Lugar nacimiento: 
              <label for="ciudad">Ciudad:</label>
              <select name="ciudad" id="ciudad">
                </select>
              <label for="Pais">Pais:</label>
              <select name="Pais" id="Pais">
                </select>
            </div></th>
            <td><input type="checkbox" name="libreta de colegio" id="libreta de colegio"> 
              <label for="libreta de colegio">Libreta de colegio</label></td>
          </tr>
          <tr> 
            <th scope="row"><div align="left"> 
              <label for="Nombre completo padre">Nombre completo padre:</label>
              <input type="text" name="Nombre completo padre" id="Nombre completo padre">
              <label for="Sexo">(*)Sexo:</label>
              <select name="Sexo" id="Sexo">
                </select>
            </div></th>
            <td>Fecha de inscripción y fotografía</td>
          </tr>
          <tr> 
            <th scope="row"><div align="left"> 
              <label for="Nombre completo padre">Nombre completo madre:</label>
              <input type="text" name="Nombre completo madre" id="Nombre completo madre">
              <label for="correo electronico">Correo electronico:</label>
              <input type="text" name="correo electronico" id="correo electronico">
            </div></th>
            <td>Desea inscribir con otra fecha</td>
          </tr>
          <tr> 
            <th scope="row"><div align="left"> 
              <label for="Direccion">Dirección:</label>
              <input type="text" name="Direccion" id="Direccion">
              <label for="Colegio">Colegio:</label>
              <input type="text" name="Colegio" id="Colegio">
            </div></th>
            <td><input type="radio" name="radio" id="Si" value="Si"> <label for="Si">Si 
              <input type="radio" name="radio" id="No" value="No">
              No</label></td>
          </tr>
          <tr> 
            <th scope="row"><div align="left"> 
              <label for="Documento inscripcion">(*)Documento inscripción:</label>
              <select name="Documento inscripcion" id="Documento inscripcion">
                </select>
              <label for="Numero">Número:</label>
              <input type="text" name="Numero" id="Numero">
              <label for="Expedido en">Expedido en:</label>
              <select name="Expedido en" id="Expedido en">
                </select>
            </div></th>
            <td><label for="fecha"></label> <select name="fecha" id="fecha">
            </select></td>
          </tr>
          <tr> 
            <th scope="row"><div align="left"> 
              <label for="Telefono domicilio">Telefono domicilio:</label>
              <input type="text" name="Telefono domicilio" id="Telefono domicilio">
              <label for="Telefono movil">Telefono movil:</label>
              <input type="text" name="Telefono movil" id="Telefono movil">
            </div></th>
            <td> <div align="center"> 
              <input type="submit" name="Tomar fotografía" id="Tomar fotografía" value="Tomar fotografía">
            </div></td>
          </tr>
          <tr> 
            <th scope="row"><div align="left">Datos de la inscripción</div></th>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <th scope="row"><div align="left"> 
              <label for="Carrera">(*)Carrera:</label>
              <select name="Carrera" id="Carrera">
                </select>
              <label for="Gestion">(*)Gestion:</label>
              <select name="Gestion" id="Gestion">
                </select>
            </div></th>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <th scope="row"><label for="Nivel"> <div align="left">(*)Nivel: 
              <select name="Nivel2" id="Nivel2">
                </select>
              (*)Turno: 
              <select name="Turno2" id="Turno2">
                </select>
              <label for="Semestre">(*)Semestre</label>
              <select name="Semestre" id="Semestre">
                </select>
              </div></label>
            </th>
            <td>&nbsp;</td>
          </tr>
          <tr> 
            <th scope="row"><div align="left"> 
              <label for="Nivel">(*)Grado instrucción:</label>
              <select name="Nivel" id="Nivel">
                </select>
              <label for="Turno">(*)Grupo:</label>
              <select name="Turno" id="Turno">
                </select>
            </div></th>
            <td><label for="Observaciones">Observaciones:</label> <textarea name="Observaciones" id="Observaciones"></textarea></td>
          </tr>
        </table>
        <p align="left"> 
          <input type="submit" name="Limpiar datos" id="Limpiar datos" value="Limpiar datos">
        </p>
      </form>
      <form action="reg_inscripcion_estnuevos.php" method="post">
        <input type="submit" name="Inscribir nuevo" id="Inscribir nuevo" value="Inscribir nuevo">
      </form>
       <form action="reg_inscripcion_estnuevos.php" method="post">
        <input type="submit" name="Añadir nuevo" id="Añadir nuevo" value="Añadir nuevo">
      </form>
      <form action="../../menu.php" method="post">
        <input type="submit" name="Salir" id="Salir" value="Salir">
      </form><td valign="top">
	<table border="1" cellpadding="0" cellspacing="0">
          <tr> 
            <td> <div align="center">
             <form action="reg_inscripcion_estnuevos.php" method="post"> 
                <input type="submit" name="Buscar estudiante" id="Buscar estudiante" value="Buscar estudiante">
                </form>
              </div></td>
          </tr>
          <tr>
            <td><div align="center"> 
            <form action="reg_inscripcion_estnuevos.php" method="post"> 
                <input type="submit" name="Informe de calificaciones" id="Informe de calificaciones" value="Informe de calificaciones">
                </form>
              </div></td>
          </tr>
          <tr>
            <td><div align="center"> 
            <form action="reg_inscripcion_estnuevos.php" method="post"> 
                <input type="submit" name="Kárdex academico" id="Kárdex academico" value="Kárdex academico">
                </form>
              </div></td>
          </tr>
          <tr>
            <td> <div align="center"> 
            <form action="reg_inscripcion_estnuevos.php" method="post"> 
                <input type="submit" name="Kárdex económico" id="Kárdex económico" value="Kárdex económico">
                </form>
              </div></td>
          </tr>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td></td>
          </tr>
        </table>
	</td>
  </tr>
</table>
</div>
</body>
</html>