<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<link href="http://www.jandj-industries.com/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<title>J and J Industries S.A. - Home</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--<link rel="stylesheet" href="estilos.css" type="text/css" media="screen" />-->
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
<link rel="stylesheet" type="text/css" href="default.css">
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">
$(document).ready(function () {	
	
	$('#nav li').hover(
		function () {
			//show its submenu
			$('ul', this).stop().slideDown(100);

		}, 
		function () {
			//hide its submenu
			$('ul', this).stop().slideUp(100);			
		}
	);
	
});
	</script>
<script type="text/javascript">
<!--
var timeout         = 300;
var closetimer		= 0;
var ddmenuitem      = 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

// close layer when click-out
document.onclick = mclose; 
// -->
</script>
<style>
.texto
{
	text-align:center;
	font-face:arial;
	font: 15px arial;
	font-weight:bold;
	
}
</style>
</head>

<body>

 <ul id="sddm">
        <li><a href="../JandJ%202013/index.php" onmouseover="mopen('m1')" onmouseout="mclosetime()">INICIO</a> 
          <div id="m1" onmouseover="mcancelclosetime()" onmouseout="mclosetime()"></div>
        </li>
        <li> <a href="../JandJ%202013/company.html" onmouseover="mopen('m2')" onmouseout="mclosetime()">ADMINISTRATIVO</a> 
          <div id="m2" onmouseover="mcancelclosetime()" onmouseout="mclosetime()"> 
            <a href="../JandJ%202013/english/company.htm">Inscripciones</a> 
            
            <a href="../JandJ%202013/english/mision.htm">Modificar inscripci�n</a>
			<a href="../JandJ%202013/english/shareholders.htm">Cobros / Ventas </a>
			<a href="../JandJ%202013/english/management.htm">Modificar Cobros/Ventas</a>
			<a href="../JandJ%202013/english/clients.htm">Eliminar Cobros / Ventas </a>
		</div>
        </li>
        <li><a href="../JandJ%202013/designers.html" onmouseover="mopen('m3')" onmouseout="mclosetime()">ACAD�MICO</a> 
          <div id="m3" onmouseover="mcancelclosetime()" onmouseout="mclosetime()"> 
            <a href="../JandJ%202013/english/briefcases.htm">Asignar becas</a>
			<a href="../JandJ%202013/english/handbags.htm">Cambiar calificaciones</a> 
            <a href="../JandJ%202013/english/handbags.htm">Convalidaciones</a>
			<a href="../JandJ%202013/english/belts.htm">Observaciones /orden de ex�men</a> 
            <a href="../JandJ%202013/english/jackets.htm">Autorizaciones</a> 
			<a href="../JandJ%202013/english/leather_f.htm">Modificar grupo del estudiante</a> 
			<a href="../JandJ%202013/english/wooden_f.htm">Registrar criterio</a>  
			<a href="../JandJ%202013/english/wooden_f.htm">Modificar criterio</a>  
			<a href="../JandJ%202013/english/wooden_f.htm">Eliminar criterio</a>  
			<a href="../JandJ%202013/english/wooden_f.htm">Registrar evaluaci�n/materia</a>  
			<a href="../JandJ%202013/english/wooden_f.htm">Modificar criterio</a> 
			<a href="../JandJ%202013/english/wooden_f.htm">Eliminar criterio</a>  
          </div>
        </li>
        <li><a href="#" onmouseover="mopen('m4')" onmouseout="mclosetime()">ECON�MICO</a> 
          <div id="m4" onmouseover="mcancelclosetime()" onmouseout="mclosetime()"> 
            <a href="../JandJ%202013/english/tanery.htm">Definir raz�n social</a> 
			<a href="../JandJ%202013/english/supersoft.htm">Modificar raz�n social</a> 
			<a href="../JandJ%202013/english/crinkled.htm">Eliminar raz�n social</a> 
			<a href="../JandJ%202013/english/distress.htm">Definir dosificaci�n</a> 
			<a href="../JandJ%202013/english/patent.htm">Modificar dosificaci�n</a> 
			<a href="../JandJ%202013/english/crocodile.htm">Eliminar dosificaci�n</a> 
			<a href="../JandJ%202013/english/ostrich.htm">Definir caja/usuario</a> 
			<a href="../JandJ%202013/english/iguana.htm">Modificar caja/usuario</a> 
			<a href="../JandJ%202013/english/buffalo.htm">Eliminar caja/usuario</a> 
		</div>
        </li>
		 <li><a href="#" onmouseover="mopen('m5')" onmouseout="mclosetime()">APLICACIONES AVANZADAS</a> 
           <div id="m5" onmouseover="mcancelclosetime()" onmouseout="mclosetime()"> 
            <a href="../JandJ%202013/wood_t.htm">Gesti�n</a> 
			<a href="../JandJ%202013/wood_c.htm">Costos</a> 
			<a href="../JandJ%202013/wood_a.htm">Grupos</a> 
			<a href="../JandJ%202013/wood_a.htm">Registrar usuarios</a> 
			<a href="../JandJ%202013/wood_a.htm">Modificar usuarios</a>
			<a href="../JandJ%202013/wood_a.htm">Eliminar usuarios</a>
			<a href="../JandJ%202013/wood_a.htm">Eliminar estudiantes</a> 
			<a href="../JandJ%202013/wood_a.htm">Registrar usuarios</a> 
			<a href="../JandJ%202013/wood_a.htm">Modificar usuarios</a> 
			<a href="../JandJ%202013/wood_a.htm">Eliminar usuarios</a> 
			<a href="../JandJ%202013/wood_a.htm">Inscripci�n casos especiales</a> 
			<a href="../JandJ%202013/wood_a.htm">Depuraci�n deudores de mensualidad</a> 
			<a href="../JandJ%202013/wood_a.htm">Actualizar matr�cula</a>
		 </div>
				
        <li><a href="#" onmouseover="mopen('m6')" onmouseout="mclosetime()">CETRALIZADORES</a> 
          <div id="m6" onmouseover="mcancelclosetime()" onmouseout="mclosetime()"> 
            <a href="../JandJ%202013/english/designers.htm">Centralizadores internos</a> 
			<a href="../JandJ%202013/english/samples.htm">Centralizadores internos 2da instancia</a> 
			<a href="../JandJ%202013/english/samples.htm">Centralizadores de DDE</a>
			<a href="../JandJ%202013/english/samples.htm">Centralizadores de DDE 2da instancia</a> 
		 </div>
		</li>
		<li><a href="../JandJ%202013/english/contact.htm" onmouseover="mopen('m7')" onmouseout="mclosetime()">REIMPRESIONES</a> 
          <div id="m7" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
		   <a href="../JandJ%202013/english/designers.htm">Reimpresi�n inscripci�n</a> 
		   <a href="../JandJ%202013/english/designers.htm">Reimpresi�n de DDE</a>
		   </div>
          </li>
          <li> <a href="../JandJ%202013/company.html" onmouseover="mopen('m8')" onmouseout="mclosetime()">PLANILLAS</a> 
          <div id="m8" onmouseover="mcancelclosetime()" onmouseout="mclosetime()"> 
            <a href="../JandJ%202013/english/company.htm">Generar planilla de calificaciones</a> 
			<a href="../JandJ%202013/english/mision.htm">Generar 2da instancias por grupo</a> 
			<a href="../JandJ%202013/english/shareholders.htm">Generar estudiantes sin calificaciones</a>
			<a href="../JandJ%202013/english/management.htm">Generar cronograma de evaluaci�n</a> 
			<a href="../JandJ%202013/english/clients.htm">Generar asiganaci�n docentes </a> 
			<a href="../JandJ%202013/english/clients.htm">Generar control cuotas </a> 
			<a href="../JandJ%202013/english/clients.htm">Generar compras ventas </a> 
			<a href="../JandJ%202013/english/clients.htm">Generar control cuotas </a> 
			<a href="../JandJ%202013/english/clients.htm">Generar control cuotas archivo</a> 
			<a href="../JandJ%202013/english/clients.htm">Carga de planillas de calificaciones</a> 
			<a href="../JandJ%202013/english/clients.htm"> Carga estudiantes sin calificaci�n</a> 
			<a href="../JandJ%202013/english/clients.htm">Carga cronograma de evaluaciones </a>
		</div>
        </li>
        <li> <a href="../JandJ%202013/company.html" onmouseover="mopen('m9')" onmouseout="mclosetime()">REPORTES</a> 
          <div id="m9" onmouseover="mcancelclosetime()" onmouseout="mclosetime()"> 
            <a href="../JandJ%202013/english/company.htm">Calificaciones</a> 
			<a href="../JandJ%202013/english/mision.htm">Acad�mico</a> 
			<a href="../JandJ%202013/english/shareholders.htm">Economico</a> 
			<a href="../JandJ%202013/english/management.htm">Listas de estudiantes</a>
		   </div>
        </li>
        <li> <a href="../JandJ%202013/company.html" onmouseover="mopen('m10')" onmouseout="mclosetime()">RECURSOS HUMANOS</a> 
          <div id="m10" onmouseover="mcancelclosetime()" onmouseout="mclosetime()"> 
            <a href="../JandJ%202013/english/company.htm">Registro de personal</a> 
			<a href="../JandJ%202013/english/mision.htm">Actualizar registro de personal</a> 
			<a href="../JandJ%202013/english/shareholders.htm">Eliminar registro de personal</a> 
			<a href="../JandJ%202013/english/management.htm">Asignar cargo</a> 
		  </div>
        </li>
        <li> <a href="../JandJ%202013/company.html" onmouseover="mopen('m11')" onmouseout="mclosetime()">DDE</a> 
          <div id="m11" onmouseover="mcancelclosetime()" onmouseout="mclosetime()"> 
            <a href="../JandJ%202013/english/company.htm">Libro de inscripciones</a>  </div>
        </li>
        </li>
      </ul>
      <div style="clear:both"></div>
 
 
  
</table>
</body>
</html>
